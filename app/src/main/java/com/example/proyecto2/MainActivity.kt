package com.example.proyecto2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)




        btnEnviar.setOnClickListener {

            val Nombres = edtNombre.text.toString()
            var Edad = edtEdad.text.toString()
            var Vacunas = ""
            var Tipo = 0


            if (Nombres.isEmpty()) {
                Toast.makeText(this, "Ingrese el nombre de su mascota", Toast.LENGTH_LONG).show()
                return@setOnClickListener


            }

            if (Edad.isEmpty()) {
                Toast.makeText(this, "Ingrese la edad de su mascota", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

          when {
                rbConejo.isChecked -> Tipo =1
                rbGato.isChecked -> Tipo =2
                rbPerro.isChecked -> Tipo =3
            }

            when {
                chkVacuna1.isChecked->Vacunas+="Distemper"+" / "

            }
            when {
                chkVacuna2.isChecked -> Vacunas += "Parvovirus" + " / "
            }
            when {
                chkVacuna3.isChecked -> Vacunas += "Hepatitis Canina" + " / "
            }
            when {
                chkVacuna4.isChecked -> Vacunas += "Leptospirosis" + " / "
            }
            when {
                chkVacuna5.isChecked -> Vacunas += "Rabia" + " / "
            }


            val bundle = Bundle().apply {

                putString("KEY_NOMBRE", Nombres)
                putString("KEY_EDAD", Edad)
               putInt("KEY_TIPO",Tipo)
              putString("KEY_VACUNAS", Vacunas)
            }



            val intent = Intent(this, Informacion::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)


        }


    }
}
