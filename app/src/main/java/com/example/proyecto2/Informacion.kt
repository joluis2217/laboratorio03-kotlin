package com.example.proyecto2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_informacion.*

class Informacion : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_informacion)

        val bundleRecepcion :Bundle? =intent.extras


        /*  conejo -1
            gato  - 2

            perro -3
            */

        bundleRecepcion?.let {
            val   nombres=it.getString("KEY_NOMBRE","")
            val   edad=it.getString("KEY_EDAD","")
            val  tipos=it.getInt("KEY_TIPO",0)
            val  vacunas=it.getString("KEY_VACUNAS","")

            tvNombreResultado.text=nombres
            tvEdadResultado.text=edad
            tvVacunasResultado.text=vacunas

            when {
                tipos==1-> imgMascotaResultado.setImageResource(R.drawable.conejo)
                tipos==2->imgMascotaResultado.setImageResource(R.drawable.cat)
                tipos==3->imgMascotaResultado.setImageResource(R.drawable.pet)
            }



        }


    }
}
